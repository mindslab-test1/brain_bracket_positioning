from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.utils.data
from torch.autograd import Variable
import numpy as np
import torch.nn.functional as F


class STN3d(nn.Module):
    def __init__(self):
        super(STN3d, self).__init__()
        self.conv1 = torch.nn.Conv1d(3, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)
        self.conv3 = torch.nn.Conv1d(128, 1024, 1)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, 9)
        self.relu = nn.ReLU()

        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.bn3 = nn.BatchNorm1d(1024)
        self.bn4 = nn.BatchNorm1d(512)
        self.bn5 = nn.BatchNorm1d(256)

    def forward(self, x):
        batchsize = x.size()[0]
        _ = self.conv1(x)
        _ = self.bn1(_)
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = torch.max(x, 2, keepdim=True)[0]
        x = x.view(-1, 1024)

        x = F.relu(self.bn4(self.fc1(x)))
        x = F.relu(self.bn5(self.fc2(x)))
        x = self.fc3(x)

        iden = Variable(torch.from_numpy(np.array([1, 0, 0, 0, 1, 0, 0, 0, 1]).astype(np.float32))).view(1, 9).repeat(
            batchsize, 1)
        if x.is_cuda:
            iden = iden.cuda()
        x = x + iden
        x = x.view(-1, 3, 3)
        return x


class STNkd(nn.Module):
    def __init__(self, k=64):
        super(STNkd, self).__init__()
        self.conv1 = torch.nn.Conv1d(k, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)
        self.conv3 = torch.nn.Conv1d(128, 1024, 1)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, k * k)
        self.relu = nn.ReLU()

        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.bn3 = nn.BatchNorm1d(1024)
        self.bn4 = nn.BatchNorm1d(512)
        self.bn5 = nn.BatchNorm1d(256)

        self.k = k

    def forward(self, x):
        batchsize = x.size()[0]
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = torch.max(x, 2, keepdim=True)[0]
        x = x.view(-1, 1024)

        x = F.relu(self.bn4(self.fc1(x)))
        x = F.relu(self.bn5(self.fc2(x)))
        x = self.fc3(x)

        iden = Variable(torch.from_numpy(np.eye(self.k).flatten().astype(np.float32))).view(1, self.k * self.k).repeat(
            batchsize, 1)
        if x.is_cuda:
            iden = iden.cuda()
        x = x + iden
        x = x.view(-1, self.k, self.k)
        return x


class PointNetfeat(nn.Module):
    def __init__(self, global_feat=True, feature_transform=False):
        super(PointNetfeat, self).__init__()
        self.stn = STN3d()
        self.conv1 = torch.nn.Conv1d(3, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)
        self.conv3 = torch.nn.Conv1d(128, 1024, 1)
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.bn3 = nn.BatchNorm1d(1024)
        self.global_feat = global_feat
        self.feature_transform = feature_transform
        if self.feature_transform:
            self.fstn = STNkd(k=64)

    def forward(self, x):
        n_pts = x.size()[2]
        trans = self.stn(x)
        x = x.transpose(2, 1)
        x = torch.bmm(x, trans)
        x = x.transpose(2, 1)
        x = F.relu(self.bn1(self.conv1(x)))

        if self.feature_transform:
            trans_feat = self.fstn(x)
            x = x.transpose(2, 1)
            x = torch.bmm(x, trans_feat)
            x = x.transpose(2, 1)
        else:
            trans_feat = None

        pointfeat = x
        x = F.relu(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))
        x = torch.max(x, 2, keepdim=True)[0]
        x = x.view(-1, 1024)
        if self.global_feat:
            return x, trans, trans_feat
        else:
            x = x.view(-1, 1024, 1).repeat(1, 1, n_pts)
            return torch.cat([x, pointfeat], 1), trans, trans_feat


class PointNetCls(nn.Module):
    def __init__(self, k=2, feature_transform=False):
        super(PointNetCls, self).__init__()
        self.feature_transform = feature_transform
        self.feat = PointNetfeat(global_feat=True, feature_transform=feature_transform)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, k)
        self.dropout = nn.Dropout(p=0.3)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.relu = nn.ReLU()

    def forward(self, x):
        x, trans, trans_feat = self.feat(x)
        x = F.relu(self.bn1(self.fc1(x)))
        x = F.relu(self.bn2(self.dropout(self.fc2(x))))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1), trans, trans_feat


class PointNetDenseCls(nn.Module):
    def __init__(self, k=2, feature_transform=False):
        super(PointNetDenseCls, self).__init__()
        self.k = k
        self.feature_transform = feature_transform
        self.feat = PointNetfeat(global_feat=False, feature_transform=feature_transform)
        self.conv1 = torch.nn.Conv1d(1088, 512, 1)
        self.conv2 = torch.nn.Conv1d(512, 256, 1)
        self.conv3 = torch.nn.Conv1d(256, 128, 1)
        self.conv4 = torch.nn.Conv1d(128, self.k, 1)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(128)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        x, trans, trans_feat = self.feat(x)
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = self.conv4(x)
        x = x.transpose(2, 1).contiguous()
        # x = F.log_softmax(x.view(-1,self.k), dim=-1)
        x = x.view(-1, n_pts, self.k)
        return x, trans, trans_feat


class PointNetDenseCls2(nn.Module):
    def __init__(self, k=2, feature_transform=False):
        super(PointNetDenseCls2, self).__init__()
        self.k = k
        self.feature_transform = feature_transform
        self.feat = PointNetfeat(global_feat=False, feature_transform=feature_transform)
        self.conv1 = torch.nn.Conv1d(1088, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, self.k, 1)
        self.bn1 = nn.BatchNorm1d(64)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        x, trans, trans_feat = self.feat(x)
        x = F.relu(self.bn1(self.conv1(x)))
        x = self.conv2(x)
        x = x.transpose(2, 1).contiguous()
        # x = F.log_softmax(x.view(-1,self.k), dim=-1)
        x = x.view(batchsize, n_pts, self.k)
        return x, trans, trans_feat


def feature_transform_regularizer(trans):
    d = trans.size()[1]
    batchsize = trans.size()[0]
    I = torch.eye(d)[None, :, :]
    if trans.is_cuda:
        I = I.cuda()
    loss = torch.mean(torch.norm(torch.bmm(trans, trans.transpose(2, 1)) - I, dim=(1, 2)))
    return loss


class axisModel(nn.Module):
    def __init__(self, t=3, k=2, feature_transform=False):
        super(axisModel, self).__init__()
        self.t = t
        self.k = k
        self.feature_transform = feature_transform
        self.seg = PointNetDenseCls(k, feature_transform)

        self.feat = PointNetfeat(global_feat=False, feature_transform=feature_transform)
        self.conv1 = torch.nn.Conv1d(1088, 512, 1)
        self.conv2 = torch.nn.Conv1d(512, 256, 1)
        self.conv3 = torch.nn.Conv1d(256, 128, 1)
        self.conv4 = torch.nn.Conv1d(128, self.k, 1)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(128)

        self.fc1 = torch.nn.Linear(4, 3)
        self.fc2 = torch.nn.Linear(2000, 500)
        self.fc3 = torch.nn.Linear(500, 200)
        self.fc4 = torch.nn.Linear(200, 80)
        self.fc5 = torch.nn.Linear(80, 30)
        self.fc6 = torch.nn.Linear(30, self.t)

        self.bn4 = nn.BatchNorm1d(3)
        self.bn5 = nn.BatchNorm1d(500)
        self.bn6 = nn.BatchNorm1d(200)
        self.bn7 = nn.BatchNorm1d(80)
        self.bn8 = nn.BatchNorm1d(30)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        y, trans, trans_feat = self.feat(x)
        y = F.relu(self.bn1(self.conv1(y)))
        y = F.relu(self.bn2(self.conv2(y)))
        y = F.relu(self.bn3(self.conv3(y)))
        y = self.conv4(y)
        y = y.transpose(2, 1).contiguous()
        # y = F.log_softmay(y.view(-1,self.k), dim=-1)
        y = y.view(batchsize, n_pts, self.k)

        pred = y.view(-1, self.k)
        pred_choice = pred.data.max(1)[1].view(batchsize, 1, n_pts).float()
        y = torch.cat((x, pred_choice), dim=1)
        y = y.transpose(1, 2)

        y = F.relu(self.bn4(self.fc1(y).transpose(1, 2)))
        y = F.relu(self.bn5(self.fc2(y).transpose(1, 2)))
        y = F.relu(self.bn6(self.fc3(y.transpose(1, 2)).transpose(1, 2)))
        y = F.relu(self.bn7(self.fc4(y.transpose(1, 2)).transpose(1, 2)))
        y = F.relu(self.bn8(self.fc5(y.transpose(1, 2)).transpose(1, 2)))
        y = self.fc6(y.transpose(1, 2)).contiguous()
        y = y.view(batchsize, self.t, 3)
        return y, (x, trans, trans_feat)


class axisModel2(nn.Module):
    def __init__(self, t=3, k=2, feature_transform=False):
        super(axisModel2, self).__init__()
        self.t = t
        self.k = k
        self.feature_transform = feature_transform
        self.seg = PointNetDenseCls(k, feature_transform)

        self.feat = PointNetfeat(global_feat=False, feature_transform=feature_transform)
        self.conv1 = torch.nn.Conv1d(1088, 512, 1)
        self.conv2 = torch.nn.Conv1d(512, 256, 1)
        self.conv3 = torch.nn.Conv1d(256, 128, 1)
        self.conv4 = torch.nn.Conv1d(128, self.k, 1)

        self.relu = nn.ReLU(inplace=True)

        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(128)
        self.bn4 = nn.BatchNorm1d(4)

        self.seq1 = [
            torch.nn.Linear(2000, 500),
            self.bn4, self.relu,
            torch.nn.Linear(500, 500),
            self.bn4, self.relu,
            torch.nn.Linear(500, 500),
            self.bn4, self.relu,
            torch.nn.Linear(500, 200),
            self.bn4, self.relu,
            torch.nn.Linear(200, 100),
            self.bn4, self.relu,
            torch.nn.Linear(100, 50),
            self.bn4, self.relu,
            torch.nn.Linear(50, self.t),
            self.bn4, self.relu,
        ]
        self.seq1 = nn.Sequential(*self.seq1)

        self.seq2 = [
            torch.nn.Linear(4, 3)
        ]
        self.seq2 = nn.Sequential(*self.seq2)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        y, trans, trans_feat = self.feat(x)
        y = F.relu(self.bn1(self.conv1(y)))
        y = F.relu(self.bn2(self.conv2(y)))
        y = F.relu(self.bn3(self.conv3(y)))
        y = self.conv4(y)
        y = y.transpose(2, 1).contiguous()
        # y = F.log_softmay(y.view(-1,self.k), dim=-1)
        y = y.view(batchsize, n_pts, self.k)

        pred = y.view(-1, self.k)
        pred_choice = pred.data.max(1)[1].view(batchsize, 1, n_pts).float()

        y = torch.cat((x, pred_choice), dim=1)

        y = self.seq1(y)
        y = torch.transpose(y, 1, 2)
        y = self.seq2(y).contiguous()

        y = y.view(batchsize, self.t, 3)
        return y, (x, trans, trans_feat)


class axisModel3(nn.Module):
    def __init__(self, t=3, k=2, feature_transform=False):
        super(axisModel3, self).__init__()
        self.t = t
        self.k = k
        self.feature_transform = feature_transform
        self.seg = PointNetDenseCls(k, feature_transform)

        self.conv0 = torch.nn.Conv1d(4, 3, 1)
        self.conv1 = torch.nn.Conv1d(2000, 500, 1)
        self.conv2 = torch.nn.Conv1d(500, 200, 1)
        self.conv3 = torch.nn.Conv1d(200, 80, 1)
        self.conv4 = torch.nn.Conv1d(80, 30, 1)
        self.conv5 = torch.nn.Conv1d(30, self.t, 1)

        self.bn0 = nn.BatchNorm1d(3)
        self.bn1 = nn.BatchNorm1d(500)
        self.bn2 = nn.BatchNorm1d(200)
        self.bn3 = nn.BatchNorm1d(80)
        self.bn4 = nn.BatchNorm1d(30)
        self.bn5 = nn.BatchNorm1d(self.t)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        y, trans, trans_feat = self.seg(x)

        pred = y.view(-1, self.k)
        pred_choice = pred.data.max(1)[1].view(batchsize, 1, n_pts).float()
        y = torch.cat((x, pred_choice), dim=1)

        y = F.relu(self.bn0(self.conv0(y)))
        y = y.transpose(1, 2)
        y = F.relu(self.bn1(self.conv1(y)))
        y = F.relu(self.bn2(self.conv2(y)))
        y = F.relu(self.bn3(self.conv3(y)))
        y = F.relu(self.bn4(self.conv4(y)))
        y = F.relu(self.bn5(self.conv5(y))).contiguous()
        return y, (x, trans, trans_feat)


class axisModel4(nn.Module):
    def __init__(self, t=3, k=1, feature_transform=False):
        super(axisModel4, self).__init__()
        self.t = t
        self.k = k
        self.feature_transform = feature_transform
        self.seg = PointNetDenseCls(k, feature_transform)

        # self.pool = nn.MaxPool1d()

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        y, trans, trans_feat = self.seg(x)

        print(y.shape)
        assert False

        pred = y.view(-1, self.k)
        pred_choice = pred.data.max(1)[1].view(batchsize, 1, n_pts).float()
        y = torch.cat((x, pred_choice), dim=1)

        y = F.relu(self.bn0(self.conv0(y)))
        y = y.transpose(1, 2)
        y = F.relu(self.bn1(self.conv1(y)))
        y = F.relu(self.bn2(self.conv2(y)))
        y = F.relu(self.bn3(self.conv3(y)))
        y = F.relu(self.bn4(self.conv4(y)))
        y = F.relu(self.bn5(self.conv5(y))).contiguous()
        return y, (x, trans, trans_feat)


class mixedModel(nn.Module):
    def __init__(self, t=3, k=2, feature_transform=False):
        super(mixedModel, self).__init__()
        self.t = t
        self.k = k
        self.feature_transform = feature_transform
        self.seg = PointNetDenseCls(k, feature_transform)

        self.conv0 = torch.nn.Conv1d(5, 3, 1)
        self.conv1 = torch.nn.Conv1d(2000, 500, 1)
        self.conv2 = torch.nn.Conv1d(500, 200, 1)
        self.conv3 = torch.nn.Conv1d(200, 80, 1)
        self.conv4 = torch.nn.Conv1d(80, 30, 1)
        self.conv5 = torch.nn.Conv1d(30, self.t, 1)

        self.bn0 = nn.BatchNorm1d(3)
        self.bn1 = nn.BatchNorm1d(500)
        self.bn2 = nn.BatchNorm1d(200)
        self.bn3 = nn.BatchNorm1d(80)
        self.bn4 = nn.BatchNorm1d(30)
        self.bn5 = nn.BatchNorm1d(self.t)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]
        y, trans, trans_feat = self.seg(x)

        # pred_choice = pred.data.max(1)[1].view(batchsize, 1, n_pts).float()
        pred = y.transpose(1, 2)
        z = torch.cat((x, pred), dim=1)

        z = F.relu(self.bn0(self.conv0(z)))
        z = z.transpose(1, 2)
        z = F.relu(self.bn1(self.conv1(z)))
        z = F.relu(self.bn2(self.conv2(z)))
        z = F.relu(self.bn3(self.conv3(z)))
        z = F.relu(self.bn4(self.conv4(z)))
        z = F.relu(self.bn5(self.conv5(z))).contiguous()
        return z, (y, trans, trans_feat)


class axisModel5(nn.Module):
    def __init__(self, t=3, batch_size=32, model_size=2000):
        super(axisModel5, self).__init__()
        self.t = t

        self.fc1 = nn.Linear(4, 128)
        self.fc2 = nn.Linear(128, 512)
        # self.fc3 = nn.Linear(512, 512)
        self.fc4 = nn.Linear(512, 128)
        self.fc5 = nn.Linear(128, 3)

        self.fc6 = nn.Linear(model_size, self.t)

        self.bn1 = nn.BatchNorm1d(model_size)
        self.bn2 = nn.BatchNorm1d(model_size)
        self.bn3 = nn.BatchNorm1d(model_size)
        self.bn4 = nn.BatchNorm1d(model_size)
        self.relu = nn.ReLU(inplace=True)

        self.pool = nn.MaxPool1d(kernel_size=3, stride=2)

        self.seq = [
            self.fc1, self.bn1, self.relu,
            self.fc2, self.bn2, self.relu,
            # self.fc3, self.bn3, self.relu,
            self.fc4, self.bn4, self.relu,
            self.fc5
        ]
        self.seq = nn.Sequential(*self.seq)

    def forward(self, x):
        batchsize = x.size()[0]
        n_pts = x.size()[2]

        x = x.transpose(1, 2)
        y = self.seq(x)
        y = y.transpose(1, 2)
        y = self.fc6(y)
        y = y.transpose(1, 2)

        return y


if __name__ == '__main__':
    sim_data = torch.rand(2, 4, 2000)

    x = torch.zeros((2, 3, 1000))
    net = PointNetDenseCls(29)
    y, _, _ = net(x)
    print(y.shape)
    assert False

    seg = axisModel5(batch_size=10, t=2)

    out = seg(sim_data)
    print('seg', out.size())

    '''
        trans = STN3d()
        out = trans(sim_data)
        print('stn', out.size())
        print('loss', feature_transform_regularizer(out))

        sim_data_64d = Variable(torch.rand(32, 64, 2500))
        trans = STNkd(k=64)
        out = trans(sim_data_64d)
        print('stn64d', out.size())
        print('loss', feature_transform_regularizer(out))

        pointfeat = PointNetfeat(global_feat=True)
        out, _, _ = pointfeat(sim_data)
        print('global feat', out.size())

        pointfeat = PointNetfeat(global_feat=False)
        out, _, _ = pointfeat(sim_data)
        print('point feat', out.size())

        cls = PointNetCls(k = 5)
        out, _, _ = cls(sim_data)
        print('class', out.size())

        seg = PointNetDenseCls(k = 3)
        out, _, _ = seg(sim_data)
        print('seg', out.size())
        '''
