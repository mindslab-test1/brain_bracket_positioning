# Bracket Positioning

## Build with docker
```
docker build --no-cache --network=host -t bracket:0.1 .
```

```
docker run -itd --gpus='"device=0"' -p 39993:39993 --ipc=host --name bracket_full bracket:0.1
```



### Requirements
`Tested at Tesla V100, uses ~4G Memory`

for inference:  
`Needs ~1.5s for 1 person(2 jaw)`


## Usage
launch server first
  
test with input files  
```
python client.py --port 39993 --fileL LOWER_JAW_FILE.STL --fileU UPPER_JAW_FILE.STL
```    


### Files
input files: [google  drive](https://drive.google.com/drive/folders/1Eiu4RUgWXH-t9zhAhPUagWdjTnuKnJY9?usp=sharing)


