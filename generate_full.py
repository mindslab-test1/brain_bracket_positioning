import argparse
import copy
import numpy as np
import open3d as o3d
import os
from sklearn.decomposition import PCA
import torch

from mesh import write_ab
from model import PointNetDenseCls


def parse():
    parser = argparse.ArgumentParser(description='Bracket generation configs')
    parser.add_argument('--device', type=str, default='cuda', help='cuda(default) or cpu')
    parser.add_argument('--num_classes', type=int, default=29)
    parser.add_argument('--path_ckpt', type=str, default='./ckpt/ckpt.pth', help='path to checkpoint')
    parser.add_argument('--full_size', type=int, default=200000)
    parser.add_argument('--model_size', type=int, default=100000)
    parser.add_argument('--output', type=str, default='./output', help='output directory path')
    parser.add_argument('--input', type=str, help='input zip file')
    args = parser.parse_args()
    return args


class dentist:
    def __init__(self, args):
        self.args = args
        self.net = PointNetDenseCls(args.num_classes).to(args.device)

        try:
            params = torch.load(args.path_ckpt, map_location=args.device)
            self.net.load_state_dict(params)
        except:
            print('given path for ckpt is wrong')

    def generate_batch(self, points):
        assert isinstance(points, torch.Tensor)

        objL = []
        objU = []
        for _ in range(len(points)):
            objL.append([])
            objU.append([])

        with torch.no_grad():
            for j in range(10):
                rnds = torch.randperm(self.args.full_size)[:self.args.model_size]

                input_points = points[:, rnds, :] - torch.mean(points[:, rnds, :], dim=1, keepdim=True)
                input_points = input_points.transpose(2, 1)
                input_points = input_points.to(self.args.device)

                pred, _, _ = self.net(input_points)
                pred = pred.detach().cpu().numpy()
                pred = np.argmax(pred, axis=2)

                for batch in range(len(pred) // 2):
                    objL[batch].append((rnds.squeeze(), pred[batch * 2]))
                    objU[batch].append((rnds.squeeze(), pred[batch * 2 + 1]))

    def generate(self, points, path_output):
        if isinstance(points, np.ndarray):
            points = torch.from_numpy(points).float().to(self.args.device)
        else:
            points = points.to(self.args.device)

        objL = []
        objU = []
        with torch.no_grad():
            for j in range(10):
                rnds = torch.randperm(self.args.full_size)[:self.args.model_size]

                # rnds = np.random.choice(self.args.full_size, self.args.model_size)
                # points_ = copy.deepcopy(points[:, rnds, :])
                points_1 = points[:, rnds, :]
                points_ = points_1 - torch.mean(points_1, dim=1, keepdim=True)
                points_ = points_.transpose(2, 1)
                points_ = points_.to(self.args.device)

                pred, _, _ = self.net(points_)
                pred = pred.detach().cpu().numpy()
                pred = np.argmax(pred, axis=2)

                objL.append((rnds.squeeze(), pred[0]))
                objU.append((rnds.squeeze(), pred[1]))

        self.make_ab(points[0], objL, 'L', path_output)
        self.make_ab(points[1], objU, 'U', path_output)

    def make_ab(self, points, objs, model_type, path_output):
        '''

        Args:
            points: points of teeth
            objs: list of tuples containing rnds and preds
            model_type: L or U, determines alignment of bracket
            path_output: dir to write bracket .obj files

        Returns:

        '''

        full_size = 200000

        if isinstance(points, torch.Tensor):
            points = points.detach().cpu().numpy().squeeze()

        # label all points
        label = np.zeros(full_size, dtype=int) - 1
        for j in range(len(objs)):
            rnds, labels = objs[j]
            inds = label[rnds] == -1
            label[rnds[inds]] = labels[inds]

        label = np.clip(label, 0, 9999)

        # labels to groups
        f_num = full_size  # TODO
        cnt = np.max(label) + 1

        g2f = np.empty(cnt, dtype=object)
        for j in range(f_num):
            group = label[j]
            if g2f[group] is None:
                g2f[group] = [j]
            else:
                g2f[group].append(j)

        for j in range(len(g2f)):
            g2f[j] = np.asarray(g2f[j])

        savedir = os.path.join(path_output, model_type)
        os.makedirs(savedir, exist_ok=True)

        for j in range(cnt // 2):
            vx = np.asarray(points[g2f[2 * j + 1]]).squeeze()
            vy = np.asarray(points[g2f[2 * j + 2]]).squeeze()

            pcax = PCA(n_components=1)
            pcax.fit(vx)
            x = pcax.components_.squeeze()

            pcay = PCA(n_components=1)
            pcay.fit(vy)
            y = pcay.components_.squeeze()

            pca = PCA(n_components=2)
            # pca.fit(np.concatenate((vx, vy)))
            pca.fit(vx)
            x, y = pca.components_

            if model_type == 'L' and y[1] < 0:
                y = -y
            if model_type == 'U' and y[1] > 0:
                y = -y
            z = np.cross(x, y)

            if abs(z[1]) > abs(y[1]):
                y = copy.deepcopy(z)
                if model_type == 'L' and y[1] < 0:
                    y = -y
                if model_type == 'U' and y[1] > 0:
                    y = -y
                z = np.cross(x, y)

            axes = (x, y, z)

            base = np.mean(vx, axis=0)
            base[2] = np.mean(vy, axis=0)[2]

            path_ab = os.path.join(savedir, '%d.obj' % j)
            write_ab(base, axes, path_ab)


def main():
    args = parse()
    d = dentist(args)

    # pathL = 'inputs/19-742_L.stl'
    # pathU = 'inputs/19-742_U.stl'
    pathL = 'samples/20-1001_L.stl'
    pathU = 'samples/20-1001_U.stl'

    stlL = o3d.io.read_triangle_mesh(pathL)
    # v = np.asarray(stlL.vertices)
    # f = np.asarray(stlL.triangles)
    # pL = np.zeros((args.full_size, 3), float)
    # pL[:len(f)] = np.mean(v[f], axis=1)

    # print(np.mean(v[f], axis=1).shape)

    v = torch.as_tensor(stlL.vertices)
    f = torch.as_tensor(stlL.triangles).long()
    pL = torch.zeros((args.full_size, 3)).float()
    pL[:len(f)] = torch.mean(v[f], dim=1)

    stlU = o3d.io.read_triangle_mesh(pathU)
    # v = np.asarray(stlU.vertices)
    # f = np.asarray(stlU.triangles)
    # pU = np.zeros((args.full_size, 3), float)
    # pU[:len(f)] = np.mean(v[f], axis=1)
    v = torch.as_tensor(stlU.vertices)
    f = torch.as_tensor(stlU.triangles).long()
    pU = torch.zeros((args.full_size, 3)).float()
    pU[:len(f)] = torch.mean(v[f], dim=1)

    # points = np.array([pL, pU])
    points = torch.cat((pL.unsqueeze(0), pU.unsqueeze(0)), dim=0)
    d.generate(points)


if __name__ == '__main__':
    main()
