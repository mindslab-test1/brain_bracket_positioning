import argparse
import grpc

from proto import bracket_full_pb2
from proto import bracket_full_pb2_grpc

CHUNK_SIZE = 2 * 1024 * 1024


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', default='localhost', type=str, help='ip address')
    parser.add_argument('--port', default=39993, type=int, help='port num')
    parser.add_argument('--output', default='client.zip', help='output path', type=str)
    parser.add_argument('--fileL', default='19-742_L.stl', help='inputL', type=str)
    parser.add_argument('--fileU', default='19-742_U.stl', help='inputU', type=str)
    args = parser.parse_args()
    return args


def get_file_chunks(filename):
    with open(filename, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE);
            if len(piece) == 0:
                return
            ch = bracket_full_pb2.STL()
            ch.STLBytes = piece
            yield ch


def get_request_stream(requestlist):
    for i in requestlist:
        yield bracket_full_pb2.Request(name=i)


class Client:
    def __init__(self, args):
        self.args = args

        self.options = [('grpc.max_message_length', CHUNK_SIZE)]
        self.address = self.args.ip + ':' + str(self.args.port)
        self.channel = grpc.insecure_channel(self.address, options=self.options)
        self.stub = bracket_full_pb2_grpc.bracket_fullStub(self.channel)

    def upload(self, in_file_name):
        chs = get_file_chunks(in_file_name)
        response = self.stub.upload(chs)
        name = response.name
        return name

    def inference(self, file_L, file_U):
        print('sending files to server: ', file_L, file_U)
        path_L = self.upload(file_L)
        path_U = self.upload(file_U)
        paths = [path_L, path_U]
        print('files sent to server path: ', paths)

        chs = get_request_stream(paths)
        response = self.stub.inference(chs)

        self.download(response.name)

    def download(self, target_name):
        print('generated output file: ', target_name)
        req = bracket_full_pb2.Request(name=target_name)
        responses = self.stub.download(req)

        path_output = self.args.output
        with open(path_output, 'wb') as f:
            for response in responses:
                chunk = response.STLBytes
                f.write(chunk)
        print('output saved at: ', path_output)


def main():
    import time
    start_time = time.time()
    args = parse()
    Client(args).inference(args.fileL, args.fileU)
    print('total time', time.time() - start_time)


if __name__ == '__main__':
    main()
