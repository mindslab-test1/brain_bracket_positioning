import numpy as np
import time
import os
import _io


class mesh(object):
    def __init__(self, lines, groups=False, faster = False, normal=True):
        '''
        :param lines: list of input lines
        :param groups:
        :param faster:
        '''
        super(mesh, self).__init__()
        self.groups = groups #TODO use this or just remove it
        self.lines = lines

        self.readlines()
        if normal:
            self.setfn()
        self.setg2f()

        if not faster:
            self.setv2f()
            self.setv2v()
            self.setf2f()


    def readlines(self):
        v = []
        vn = []
        f = []
        f2g = []
        fn_index = []
        others = []
        self.cnt = 1
        current_group = 0
        for i in range(len(self.lines)):
            line = self.lines[i].strip().split(' ')
            if line[0] == 'v':
                v.append(line[1:])
            elif line[0] == 'vn':
                vn.append(line[1:])
            elif line[0] == 'f':
                for a in line[1:]:
                    f.append(a.split('/')[0])
                fn_index.append(line[-1].split('/')[-1])
                f2g.append(current_group)
            else:
                if line[0] == 'g':
                    try:
                        current_group = int(line[1])
                    except ValueError:
                        try:
                            current_group = int(''.join(line[1][7:]))
                        except IndexError as e:
                            print("group name error while reading mesh", e)
                    '''
                    if line[1][:7]== 'mmGroup':
                        current_group = int(''.join(line[1][7:]))
                    else:
                        current_group = int(line[1])
                    '''
                    if current_group >= self.cnt:
                        self.cnt = current_group+1
                others.append(line)

        self.v = np.array(v, dtype=float)
        self.vn = np.array(vn, dtype=float)
        self.f = np.array(f, dtype=int).reshape((-1, 3))
        self.fn_index = np.array(fn_index, dtype=int)

        self.f -= 1
        self.fn_index -= 1
        self.f2g = np.array(f2g, dtype=int)
        self.f2g -= np.min(self.f2g)

    def setfn(self):
        f_num = len(self.f)
        self.fn = np.zeros((f_num, 3), dtype=float)
        for i in range(f_num):
            self.fn[i] = self.vn[self.fn_index[i]]

    def setv2f(self):
        v_num = len(self.v)
        f_num = len(self.f)
        self.v2f = np.empty(v_num, dtype=object)
        for i in range(f_num):
            face = self.f[i]
            if face is not None:
                for v in face:
                    if self.v2f[v] is None:
                        self.v2f[v] = np.array([i])
                    else:
                        self.v2f[v] = np.append(self.v2f[v], i)

    def setf2f(self):
        f_num = len(self.f)
        self.f2f = np.empty(f_num, dtype=object)
        for i in range(f_num):
            s = set()
            face = self.f[i]
            for v in face:
                s.update(self.v2f[v])
            self.f2f[i] = np.array(list(s), dtype=int)

    def setv2v(self):
        v_num = len(self.v)
        self.v2v = np.empty(v_num, dtype=object)
        for i in range(v_num):
            s = set()
            faces = self.v2f[i]
            if faces is not None:
                for f in faces:
                    s.update(self.f[f])
                self.v2v[i] = np.array(list(s), dtype=int)

    def setg2f(self):
        f_num = len(self.f)
        self.g2f = np.empty(self.cnt, dtype=object)
        for i in range(f_num):
            group = self.f2g[i]
            if self.g2f[group] is None:
                self.g2f[group] = np.array([i])
            else:
                self.g2f[group] = np.append(self.g2f[group], i)

    def setv2g(self):
        v_num = len(self.v)
        self.v2g = np.zeros(v_num, dtype=int) - 1
        for i in range(v_num):
            faces = self.v2f[i]
            if faces is not None:
                groups = self.f2g[faces]
                groups = np.array(groups, dtype=int)
                counts = np.bincount(groups)
                self.v2g[i] = np.argmax(counts)

    def tolines(self):
        v_lines = self.v_lines()
        vn_lines = self.vn_lines()
        f_lines = self.f_lines_all()
        lines = v_lines+vn_lines+f_lines
        return lines

    def v_lines(self):
        lines = []
        for i in range(len(self.v)):
            point = self.v[i]
            line = 'v %f %f %f\n' % tuple(point)
            lines.append(line)
        return lines

    def vn_lines(self):
        lines = []
        for i in range(len(self.vn)):
            normal = self.vn[i]
            line = 'vn %f %f %f\n' % tuple(normal)
            lines.append(line)
        return lines

    def f_lines(self, num):
        lines = []
        if self.g2f[num] is not None:
            lines.append('g %d\n' % num)
            for j in self.g2f[num]:
                face = self.f[j]
                line = 'f %d//%d %d//%d %d//%d\n' % (
                    face[0] + 1, self.fn_index[j] + 1,
                    face[1] + 1, self.fn_index[j] + 1,
                    face[2] + 1, self.fn_index[j] + 1
                )
                lines.append(line)
        return lines

    def f_lines_all(self):
        lines = []
        for i in range(len(self.g2f)):
            lines += self.f_lines(i)
        return lines

    def lighten_group(self):
        cnt = 0
        tot = 0
        for i in range(len(self.g2f)):
            if self.g2f[i] is not None:
                tot += len(self.g2f[i])
                cnt = cnt + 1
                for j in self.g2f[i]:
                    self.f2g[j] = cnt
        self.cnt = cnt+1
        self.setg2f()
        print('tot', tot)


def lighten(lines):
    m = mesh(lines, faster=True)

    v = set()
    for face in m.f:
        v.update(face)
    v = np.sort(list(v))

    vn = set()
    for normal in m.fn_index:
        vn.add(normal)
    vn = np.sort(list(vn))

    out_lines = []
    for index in v:
        line = 'v %f %f %f\n' % tuple(m.v[index])
        out_lines.append(line)
    for index in vn:
        line = 'vn %f %f %f\n' % tuple(m.vn[index])
        out_lines.append(line)
    cnt = 0

    for i in range(len(m.g2f)):
        if m.g2f[i] is not None:
            out_lines.append('g %d\n'%cnt)
            for j in m.g2f[i]:
                face = m.f[j]
                fn_index = np.where(vn == m.fn_index[j])[0]
                # TODO multiple using of np.where will consume time
                line = 'f %d//%d %d//%d %d//%d\n' % (
                    np.where(v == face[0])[0]+1, fn_index+1,
                    np.where(v == face[1])[0]+1, fn_index+1,
                    np.where(v == face[2])[0]+1, fn_index+1,
                )
                out_lines.append(line)
            cnt = cnt + 1
    return out_lines

def parse_gt(input_lines):
    '''
    parse gt lines to point, axis_x, y, z, axes_scale
    returns point, axis x, y, z
    :param input_lines:
    :return:
    '''
    assert len(input_lines) == 6
    data = []
    for i in range(len(input_lines)-2):
        line = input_lines[i].strip().split(' ')
        data.append(line[1:])
    data = np.array(data, dtype=float)
    #data[1:4] += data[0]
    return data[:4]

class pcl(object):
    def __init__(self, lines):
        super(pcl, self).__init__()
        self.lines = lines
        self.readlines()

    def readlines(self):
        v = []
        for i in range(len(self.lines)):
            line = self.lines[i].strip().split(' ')
            if line[0] == 'v':
                v.append(line[1:])
        self.v = np.array(v, dtype=float)

def write_square(base, axes, scale, index):
    '''
    :param base:
    :param axes: x, y, z axes
    :param scale: scale of x, y, z axes
    :param file: file to write answer mesh
    :param index: n'th object cube
    :return: list of lines to write
    '''
    lines = []

    x_ = axes[0] * scale[0]
    y_ = axes[1] * scale[1]
    z_ = axes[2] * scale[2]

    LU = base - x_ + y_
    RU = base + x_ + y_
    LD = base - x_ - y_
    RD = base + x_ - y_
    Lab = np.array((LU + z_, RU + z_, LD + z_, RD + z_))
    Lin = np.array((LU - z_, RU - z_, LD - z_, RD - z_))

    for j in range(4):
        line = 'v %f %f %f\n' % tuple(Lab[j])
        lines.append(line)
    for j in range(4):
        line = 'v %f %f %f\n' % tuple(Lin[j])
        lines.append(line)

    # THE ORFER OF VERTICES IN A FACE MATTERS
    order = np.array([
        1,3,2,        2,3,4,
        5,6,7,        6,8,7,
        1,2,5,        5,2,6,
        4,3,7,        8,4,7,
        3,1,5,        7,3,5,
        2,4,6,        4,8,6
    ]).reshape((-1, 3))

    for i in range(len(order)):
        line = 'f %d %d %d\n' % (
            order[i, 0] + index, order[i, 1] + index, order[i, 2] + index
        )
        lines.append(line)

    '''
    lines.append('f %d %d %d\n' % (1 + index, 3 + index, 2 + index))
    lines.append('f %d %d %d\n' % (2 + index, 3 + index, 4 + index))
    lines.append('f %d %d %d\n' % (5 + index, 6 + index, 7 + index))
    lines.append('f %d %d %d\n' % (6 + index, 8 + index, 7 + index))
    lines.append('f %d %d %d\n' % (1 + index, 2 + index, 5 + index))
    lines.append('f %d %d %d\n' % (5 + index, 2 + index, 6 + index))
    lines.append('f %d %d %d\n' % (4 + index, 3 + index, 7 + index))
    lines.append('f %d %d %d\n' % (8 + index, 4 + index, 7 + index))
    lines.append('f %d %d %d\n' % (3 + index, 1 + index, 5 + index))
    lines.append('f %d %d %d\n' % (7 + index, 3 + index, 5 + index))
    lines.append('f %d %d %d\n' % (2 + index, 4 + index, 6 + index))
    lines.append('f %d %d %d\n' % (4 + index, 8 + index, 6 + index))
    '''
    return lines

def write_donut(base, axes, scale1, scale2, index):
    '''
    Args:
        base:
        axes:
        scale1: small box scale
        scale2: big box scale
        file:
        index:

    Returns: list of lines to write
    '''
    lines = []

    x_1 = axes[0] * scale1[0]
    y_1 = axes[1] * scale1[1]
    z_1 = axes[2] * scale1[2]

    x_2 = axes[0] * scale2[0]
    y_2 = axes[1] * scale2[1]
    z_2 = axes[2] * scale2[2]

    box = np.zeros((2, 2, 2, 2, 3), dtype=float)
    box += base
    box[0, 0, :, :] += y_1
    box[0, 1, :, :] += y_2
    box[1, 0, :, :] -= y_1
    box[1, 1, :, :] -= y_2

    box[:, 0, :, 0] += x_1
    box[:, 1, :, 0] += x_2
    box[:, 0, :, 1] -= x_1
    box[:, 1, :, 1] -= x_2

    box[:, 0, 0, :] += z_1
    box[:, 1, 0, :] += z_2
    box[:, 0, 1, :] -= z_1
    box[:, 1, 1, :] -= z_2

    box = box.reshape(-1, 3)

    for i in range(len(box)):
        line = 'v %f %f %f\n'%tuple(box[i])
        lines.append(line)

    # THE ORFER OF VERTICES IN A FACE MATTERS
    ind = index

    def writer(tup):
        a, b, c = tup
        line = 'f %d %d %d\n' % (a + ind, b + ind, c + ind)
        lines.append(line)
    def writer2(tup):
        a, b, c = tup
        line = 'f %d %d %d\n' % (a + ind, c + ind, b + ind)
        lines.append(line)

    top = [
        2, 5, 1,        2, 6, 5,
        4, 6, 2,        4, 8, 6,
        3, 8, 4,        3, 7, 8,
        1, 7, 3,        1, 5, 7
             ]
    top = np.array(top).reshape((-1, 3))
    for idxs in top:
        line = 'f %d %d %d\n' % (
            idxs[0] + ind, idxs[2] + ind, idxs[1] + ind
        )
        lines.append(line)
        #writer2(tuple(i))

    bottom = top + 8
    for idxs in bottom:
        line = 'f %d %d %d\n' % (
            idxs[0] + ind, idxs[1] + ind, idxs[2] + ind
        )
        lines.append(line)
        #writer(tuple(i))

    inside = [
        2, 1, 9,        2, 9, 10,
        4, 2, 10,        4, 10, 12,
        3, 4, 12,        3, 12, 11,
        1, 3, 11,        1, 11, 9
    ]
    inside = np.array(inside).reshape((-1, 3))
    for idxs in inside:
        line = 'f %d %d %d\n' % (
            idxs[0] + ind, idxs[2] + ind, idxs[1] + ind
        )
        lines.append(line)
        # writer2(tuple(i))

    outside = inside + 4
    for idxs in outside:
        line = 'f %d %d %d\n' % (
            idxs[0] + ind, idxs[1] + ind, idxs[2] + ind
        )
        lines.append(line)
        #writer(tuple(i))
    return lines

def write_ab(base, axes, path):
    '''
    Args:
        base: center of ab-object
        axes: x, y, z axis of ab-object
        path: path to save file
    Returns: None
    '''

    lines = []
    file = open(path, 'w')
    scale = (0.1, 4.205, 4)
    lines += write_square(base, axes, scale, 0)
    scale = (3.154, 0.1, 4)
    lines += write_square(base, axes, scale, 8)

    x, y, z = axes
    base2 = base + 2.10 * y
    scale1 = (3.154, 0.05, 4)
    scale2 = (2.10, 0.05, 2.857)
    lines += write_donut(base2, axes, scale1, scale2, 16)

    base2 = base + 3.15 * y
    lines += write_donut(base2, axes, scale1, scale2, 32)

    file.writelines(lines)
    file.close()

if __name__ == '__main__':
    pass
















