import argparse
from concurrent import futures
import glob
import grpc
import logging
import os
import shutil
import time
import zipfile
import random

import numpy as np
import open3d as o3d
import torch

from proto import bracket_full_pb2
from proto import bracket_full_pb2_grpc
from generate_full import dentist

CHUNK_SIZE = 2 * 1024 * 1024


def parse():
    parser = argparse.ArgumentParser(description='Bracket generation configs')
    parser.add_argument('--device', type=str, default='cuda', help='cuda(default) or cpu')
    parser.add_argument('--num_classes', type=int, default=29)
    parser.add_argument('--path_ckpt', type=str, default='./ckpt/ckpt.pth', help='path to checkpoint')
    parser.add_argument('--full_size', type=int, default=200000)
    parser.add_argument('--model_size', type=int, default=100000)
    parser.add_argument('--output', type=str, default='./output', help='output directory path')
    parser.add_argument('--input', type=str, help='input zip file')
    parser.add_argument('--port', type=int, default=39993)

    parser.add_argument('--log_level', type=str, default='INFO', help='logger level (default: INFO)')
    args = parser.parse_args()
    return args


class bracket_fullServer(bracket_full_pb2_grpc.bracket_fullServicer):
    def __init__(self, args, logger):
        self.server_temp = './server_temp'
        os.makedirs(self.server_temp, exist_ok=True)
        self.args = args

        self.logger = logger

        self.d = dentist(self.args)

        self.logger.info(f'server started at port {self.args.port}')

    def upload(self, request_iterator, context):
        tmp_file = os.path.join(self.server_temp, str(random.randrange(1, 10000000)) + '.stl')
        with open(tmp_file, 'wb') as f:
            file_size = 0
            for request in request_iterator:
                f.write(request.STLBytes)
                file_size += len(request.STLBytes)
                if file_size > 100 * 1024 * 1024:  # 100MB
                    tmp_file = 'too big file!'
                    break

        self.logger.debug(f'saved stl file to path: {tmp_file}')

        return bracket_full_pb2.Request(name=tmp_file)

    def download(self, request, context):
        self.logger.debug(f'returning output file of path: {request.name}')
        with open(request.name, 'rb') as f:
            while True:
                piece = f.read(CHUNK_SIZE);
                if len(piece) != 0:
                    stl = bracket_full_pb2.STL()
                    stl.STLBytes = piece
                    yield stl
                else:
                    break

    def inference(self, request_iterator, context):
        files = []
        for req in request_iterator:
            files.append(req.name)
        file_L = files[0]
        file_U = files[1]

        uid_inference = str(random.randrange(1, 10000000))
        path_output = os.path.join(self.server_temp, uid_inference)
        os.makedirs(path_output, exist_ok=True)
        self.logger.debug(f'input files: {files}')

        stlL = o3d.io.read_triangle_mesh(file_L)
        v = torch.as_tensor(stlL.vertices)
        f = torch.as_tensor(stlL.triangles).long()
        pL = torch.zeros((self.args.full_size, 3)).float()
        pL[:len(f)] = torch.mean(v[f], dim=1)

        stlU = o3d.io.read_triangle_mesh(file_U)
        v = torch.as_tensor(stlU.vertices)
        f = torch.as_tensor(stlU.triangles).long()
        pU = torch.zeros((self.args.full_size, 3)).float()
        pU[:len(f)] = torch.mean(v[f], dim=1)

        points = torch.cat((pL.unsqueeze(0), pU.unsqueeze(0)), dim=0)

        self.logger.debug(f'inferencing files: {files}')
        self.d.generate(points, path_output)

        def zipdir(path, ziph):
            # ziph is zipfile handle
            for root, dirs, files in os.walk(path):
                for file in files:
                    ziph.write(os.path.join(root, file))

        path_zip = os.path.join(self.server_temp, uid_inference + '.zip')
        zipf = zipfile.ZipFile(path_zip, 'w', zipfile.ZIP_DEFLATED)
        zipdir(path_output, zipf)
        zipf.close()

        self.logger.debug(f'output generated at path: {path_zip}')

        ret = bracket_full_pb2.Request(name=path_zip)
        return ret

    def check_files(self):
        self.logger.info('deleting old files')

        path_files = sorted(glob.glob(os.path.join(self.server_temp, '*')))

        time_now = time.time()
        for path_file in path_files:
            mtime = os.path.getmtime(path_file)
            if time_now - mtime > 24 * 60 * 60:  # deleting old files every day
                try:
                    shutil.rmtree(path_file, ignore_errors=True)
                    os.remove(path_file)
                except:
                    pass
                self.logger.debug(f'deleted old files under {path_file}')


def serve():
    args = parse()

    logging.basicConfig(level=args.log_level.upper())
    logger = logging.getLogger('bracket_positoning')

    bracket_server = bracket_fullServer(args=args, logger=logger)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1, ))
    bracket_full_pb2_grpc.add_bracket_fullServicer_to_server(bracket_server, server)
    server.add_insecure_port('[::]:%d' % args.port)
    server.start()

    try:
        while True:
            bracket_server.check_files()
            time.sleep(60 * 60)  # check for deleting old files every hour
    except KeyboardInterrupt:
        logger.info('KeyboardInterrupt, stopping server')
        server.stop(0)


if __name__ == '__main__':
    serve()
