FROM docker.maum.ai:443/brain/vision:v0.2.0

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    sudo \
    gnupg \
    libgl1-mesa-dev

RUN python -m pip --no-cache-dir install --upgrade pip

ADD ./requirements.txt /root/brain_bracket_positioning/requirements.txt

RUN python -m pip --no-cache-dir install -r /root/brain_bracket_positioning/requirements.txt

RUN python -m pip --no-cache-dir install torch==1.6.0+cu101 torchvision==0.7.0+cu101 -f https://download.pytorch.org/whl/torch_stable.html

COPY . /root/brain_bracket_positioning

WORKDIR /root/brain_bracket_positioning

RUN python -m pip --no-cache-dir install --upgrade grpcio grpcio-tools protobuf && \
    python -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./proto/bracket_full.proto && \
    ldconfig && \
    rm -rf /tmp/ /workspace/

EXPOSE 39993

RUN mkdir ckpt

RUN wget -q --no-check-certificate 'https://docs.google.com/uc?export=download&id=10KCKgMAs0bjZ5MnwVHl-vwSJ2-beEVJk' -O ckpt/ckpt.pth

ENTRYPOINT ["python", "server.py"]
